import unittest
from unittest.mock import patch, MagicMock
from handlers.pull_requests import get_pull_requests


class TestRequests(unittest.TestCase):
    @patch('handlers.pull_requests.requests')
    def test_requests(self, mock_request):
        mock_response = MagicMock(status_code=200)
        mock_response.json.return_value = [
            {"number": 1,
             "title": "title",
             "url": "http://ahrybovich.com",
             "state": "open",
             "labels": [
                 {"name": "needs-review"}]
             }
        ]
        mock_request.get.return_value = mock_response

        expected = [{"title": "open", "num": 1,
                     "link": "http://ahrybovich.com"}]
        result = get_pull_requests("needs-review")
        self.assertEqual(result, expected)

    @patch('handlers.pull_requests.requests')
    def test_req_fail(self, mock_request):
        mock_response = MagicMock(status_code=404)
        mock_request.get.return_value = mock_response
        result = get_pull_requests("open")
        self.assertEqual(result, [])


if __name__ == "__main__":
    unittest.main()
