import requests
import os

TOKEN = os.getenv("TOKEN")
HEADERS = {"Authorization": f"Bearer {TOKEN}"}
API_URL = "https://api.github.com/repos/boto/boto3/pulls"
PARAMS = {
    "state": "",
    "per_page": 100
}


def label_find(label: list, state):
    labeled = False
    if state == "open" or state == "closed":
        return True
    for i in label:
        if i["name"] == f"{state}":
            labeled = True
    return labeled


def requestor(state):
    if state == "bug":
        PARAMS["state"] = "all"
    else:
        PARAMS["state"] = state
    answer = requests.get(API_URL, headers=HEADERS, params=PARAMS)
    try:
        if answer.ok:
            return [i for i in answer.json() if label_find(i["labels"], state)]
    except Exception as e:
        print(e)


def get_pull_requests(state: str):
    """
    Example of return:
    [
        {
        "title": "Add useful stuff",
        "num": 56,
        "link": "https://github.com/boto/boto3/pull/56"},
        {
        "title": "Fix something",
        "num": 57,
        "link": "https://github.com/boto/boto3/pull/57"},
    ]
    """
    print("Token:", TOKEN)
    state_info = requestor(state)
    answer = []
    try:
        if len(state_info) != 0:
            answer = [{"title": a["state"],
                       "num": a["number"],
                       "link": a["url"]}
                      for a in state_info]
    except Exception as e:
        print(e)
    return answer
